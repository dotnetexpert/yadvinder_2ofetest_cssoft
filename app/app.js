var app;
(function (app) {    
    var main =angular.module('app.profile', ['ngMaterial', 'ui.router'])
    // .config(["$stateProvider", "$urlRouterProvider", 
    main.config(routeConfig);
        routeConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
    function routeConfig($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state('/', {
    url: '/',
                templateUrl: 'components/profile/profile.html',
                controller : 'ProfileController as vm',
                bodyClass:'profile'
    });

    $urlRouterProvider.otherwise('/');
    }

})(app || (app = {}));