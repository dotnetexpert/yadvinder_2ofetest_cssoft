(function () {
    'use strict';


    function AddUserDialogController($mdDialog) {
        var vm = this;

        // Methods
        vm.closeDialog = closeDialog;
        vm.addUser = addUser;
        //////////

        function closeDialog() {            
            $mdDialog.hide();
        }

        function addUser() {
          
        }
    }

    angular
       .module('app.profile')
       .controller('AddUserDialogController', ['$mdDialog', AddUserDialogController]);
})();
