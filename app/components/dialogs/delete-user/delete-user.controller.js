(function () {
    'use strict';

   
    /** @ngInject */
    function DeleteUserDialogController($mdDialog) {
        var vm = this;

        // Data

        //////////

        // Methods
        vm.closeDialog = closeDialog;
        vm.deleteUser = deleteUser;

        //////////

        function closeDialog() {
           
            $mdDialog.hide(0);
        }

        function deleteUser() {
            
            $mdDialog.hide(1);
        }
    }

    angular
        .module('app.profile')
        .controller('DeleteUserDialogController', ['$mdDialog', DeleteUserDialogController]);

})();
