﻿function ZmProfileUserController($mdDialog,$mdToast, $document,zmProfileUserService,$scope) {
    var vm = this;

    vm.AddUserDialog = AddUserDialog;
    vm.DeleteUserDialog = DeleteUserDialog;
    vm.openDetailView = openDetailView;

     zmProfileUserService.getListOfUser().then(function (response) {
        vm.List=response.data;
        },function (error) {
        
    });
    
    function openDetailView(entry) {
        $scope.$emit('passSeletedDtaa', entry);
          }

    function AddUserDialog(ev) {
        var parentEl = angular.element(document.body);
        $mdDialog.show({
            parent: parentEl,
            scope: $scope,        // use parent scope in template
            preserveScope: true,
            controller: AddUserDialogController,
            templateUrl: '../components/dialogs/add-user/add-user.html',
            targetEvent: ev,
            clickOutsideToClose: false,
            locals: { employee: 0 }
        });
        function AddUserDialogController($scope, $mdDialog, employee) {
            $scope.employee = employee;
            $scope.closeDialog = function () {
                $mdDialog.hide();
            };
            $scope.addUser = function (name) {
                if(name){
                var user = {
                    "username": name,
                    "userid": vm.List.length + 1,
                    "company": "Zippel Media",
                    "job": "Developer",
                    "location": "Elsdorf", "province": "North-Rhine-Westfalia",
                    "avatar": "./images/avatars/NoImage.jpg",
                    "type": "user"
                }
                vm.List.unshift(user);                
                $mdDialog.hide();
                $mdToast.show(
                    $mdToast.simple()
                    .content("User Added successfully...")
                    .action('OK')
                    .highlightAction(false)
                    .position('right')
            
             );
                }
            };
            
             $scope.closeDialog = function () {               
                $mdDialog.hide();                
            };
        }
    }
   
    function DeleteUserDialog(ev,id) {        
        $mdDialog.show({
            controller: 'DeleteUserDialogController',
            controllerAs: 'vm',
            templateUrl: '../components/dialogs/delete-user/delete-user.html',
            targetEvent: ev,
            clickOutsideToClose: true
        }).then(function(val) {
            if(val==1){
          vm.List.splice(id,1);
             $mdToast.show(
                    $mdToast.simple()
                    .content("User deleted successfully...")
                    .action('OK')
                    .highlightAction(false)
                    .position('right')
            
             );
            }
            }, function() {
               
            });
        
    };
    
}
angular.module('app.profile').controller('ZmProfileUserController', ['$mdDialog','$mdToast','$document', 'zmProfileUserService','$scope', ZmProfileUserController]);

angular.module('app.profile').component('zmProfileUser', {
    templateUrl: '../components/tabs/user/zmprofileuser.component/zmprofileuser.html',
    controller: 'ZmProfileUserController'
});

