﻿(function () {
    'use strict';

    angular
        .module('app.profile').factory('zmProfileUserService', function ($http, $q) {

            var seletedUser = [];
            var getListOfUser = function () {
                var deferred = $q.defer();
                //http.get('../contacts.json');
                $http.get('../contacts.json'          
                ).success(function (data) {
                    deferred.resolve(data);
                }).error(function (error) {
                    deferred.reject(error);
                    console.log("error");
                });
                return deferred.promise;
            }

            var setSelectedUserddata = function (data) {
                seletedUser=[];
                seletedUser.push(data);
            }
    
            var getSelectedUserddata=function myfunction() {
                return seletedUser;
            }
    return {
        getListOfUser: getListOfUser,
        setSelectedUserddata: setSelectedUserddata,
        getSelectedUserddata: getSelectedUserddata
          };
});


})();