var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');

var browserSync = require('browser-sync').create();


gulp.task('sass', function() {
  return gulp.src('all.scss') // Gets all files ending with .scss in app/scss and children dirs
    .pipe(sass())
    .pipe(gulp.dest('./lib/css'))
});

gulp.task('serve', function() {
  browserSync.init({
    server: {
      baseDir: 'app',     
    },
  })
});